# Code Guidelines

## General

* Use meaningful variable and function names that describe what they do.
* Avoid using global variables.
* Comment your code to explain what it does.
* Follow the established coding conventions of your language.

## Formatting

* Indent your code with four spaces.
* Put spaces around operators and after commas.
* Use blank lines to separate sections of code.

## Testing

* Test your code thoroughly to ensure it works as intended.
* Write unit tests for all functions and classes.
* Write integration tests to ensure all components work together properly.
